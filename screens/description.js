import React from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';

const Description = ({navigation, route}) => {
  const {img, imgArea} = styles;
  return (
    <>
      <View style={imgArea}>
        <Image style={img} source={{uri: route.params.userImg}} />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  imgArea: {display: 'flex', width: '100%'},
  img: {
    width: '100%',
    height: '100%',
  },
});

export default Description;

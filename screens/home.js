import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  View,
  Text,
  Image,
  ActivityIndicator,
  FlatList,
} from 'react-native';

const Home = ({navigation}) => {
  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    fetch(
      'https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0',
    )
      .then((res) => res.json())
      .then((json) => setData(json))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  const {itemBox, itemArea, img, text, name, textDescription} = styles;
  const nav = (i) => {
    navigation.navigate('Description', {userImg: i});
  };

  return (
    <SafeAreaView>
      <View style={itemArea}>
        {isLoading ? (
          <ActivityIndicator />
        ) : (
          <FlatList
            data={data}
            renderItem={({item}) => {
              return (
                <TouchableOpacity
                  style={itemBox}
                  onPress={() => nav(item.urls.full)}>
                  <Image style={img} source={{uri: item.urls.small}} />

                  <View style={text}>
                    <Text style={name}>Autor : {item.user.name}</Text>

                    <Text style={textDescription}>
                      description : {item.description}
                    </Text>
                  </View>
                </TouchableOpacity>
              );
            }}
            keyExtractor={(item) => item.id}
          />
        )}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  itemBox: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: 120,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 10,
    backgroundColor: 'white',
    marginBottom: 20,
    overflow: 'hidden',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  itemArea: {
    display: 'flex',
    padding: 15,
  },

  img: {
    width: 120,
    height: 120,
  },
  name: {fontSize: 18, overflow: 'hidden', width: 200},
  text: {marginLeft: 50, display: 'flex', flexDirection: 'column'},
  textDescription: {
    display: 'flex',
    width: 200,
    marginTop: 10,
    height: 35,
    overflow: 'hidden',
  },
});

export default Home;

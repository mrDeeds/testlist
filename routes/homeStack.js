import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Home from '../screens/home';
import Description from '../screens/description';

const Stack = createStackNavigator();

function RootStack() {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Description"
        component={Description}
        options={{title: 'mainPhoto'}}
      />
    </Stack.Navigator>
  );
}

export default RootStack;

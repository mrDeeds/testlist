/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import RootStack from './routes/homeStack';
import {NavigationContainer} from '@react-navigation/native';

const App = () => {
  const {block} = styles;
  return (
    <NavigationContainer>
      <RootStack />
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  block: {
    display: 'flex',
    backgroundColor: '#FFFFFF',
    padding: 15,
  },
});

export default App;
